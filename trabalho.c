#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#define ARQ_IN "commands.dat"
#define ARQ_OUT "commands.txt"
#define MAX_LINHA 8 // número de comandos.
#define MAX_LENGHT 15// número de caracteres maximo do comando.

void ClearScreen() // função limpar tela.
{
    const char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J-@"; 
    write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}

float calculocosseno(char *input, char *cmd) //função que calcula cosseno dos vetores.
{
    char copycmd[MAX_LENGHT], cpinput[MAX_LENGHT];
    int i;
    float numerador, denominador1, denominador2, cosseno;
    //variaveis e caracteres definidas para a função.

    numerador = 0;
    denominador1 = 0;
    denominador2 = 0;

    memset(cpinput,'\0', MAX_LENGHT);
    memset(copycmd, '\0', MAX_LENGHT);

    strcpy(copycmd, cmd);
    strcpy(cpinput, input);
    printf("O comando copiado e: %s\n", cpinput);
    printf("O comando copiado da lista e: %s\n", copycmd);
    
      for (i = 0; i <MAX_LENGHT; i++)
      {
          numerador += cpinput[i]*copycmd[i];
          denominador1 += pow(cpinput[i], 2);
          denominador2 += pow(copycmd[i], 2);
      }
    
       cosseno = numerador/ (sqrt(denominador1)* sqrt(denominador2)); //cosseno dos vetores.
       return cosseno;

}

//função para leitura do arquivo.
void readfile(char *filename, char *comando, int *codigo){

    FILE *arquivo_in, *arquivo_out;

    int j;
    char linha;

    arquivo_in= fopen(filename, "r");
    
    j=0;
    linha = fscanf(arquivo_in, "%s %x", comando[j], &codigo[j]);
    while (linha!= EOF)
    {
        j++;
        linha=fscanf(arquivo_in, "%s %x", comando[j], &codigo[j]);

    }

}

//função main que imprime o comando com o cosseno.
int main(){

    char comando[MAX_LENGHT], input[MAX_LENGHT], linha, copycomando[MAX_LENGHT], copyinput[MAX_LENGHT];
    int confirma, i, j, codigo[MAX_LINHA];
    float cosseno[MAX_LINHA],

    ClearScreen;

    readfile(ARQ_IN, comando, codigo);

    while (1)
   {
        printf("digite o comando:");
        scanf("%s", input);

        for (i = 0; i <MAX_LINHA; i++)
        {
        cosseno[i] = calculocosseno(input, comando[i]);
        printf("comando %d é %.1f\n", i, cosseno[i]*100); //cosseno em porcentagem.
        }
    
    }
  



}